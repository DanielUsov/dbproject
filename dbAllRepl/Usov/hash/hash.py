from itertools import product
from Crypto.Hash import keccak

alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()+'

similar_hash = {'f0bb2935bb036a2dbe0b3faf906c24992c5dec1c239b7de46e9794a9',
          '190f9f80ad1c17c2fd05ae4671c98318ecb1f933f97ef541ef6947d1'}

pass_length = 0

while len(similar_hash) > 0:
    pass_length += 1
    combinations = product(alphabet, repeat=pass_length)

    for combination in combinations:
        password = ''.join(combination)

        my_hash = keccak.new(data=password.encode('utf-8'), digest_bits=224).hexdigest()

        if my_hash in similar_hash:
            print(f'password = {password}, hash = {my_hash}')
            similar_hash.remove(my_hash)

        if len(similar_hash) == 0:
            break


# password = Dq, hash = 190f9f80ad1c17c2fd05ae4671c98318ecb1f933f97ef541ef6947d1
# password = cPFo, hash = f0bb2935bb036a2dbe0b3faf906c24992c5dec1c239b7de46e9794a9