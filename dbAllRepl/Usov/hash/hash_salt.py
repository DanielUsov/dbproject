from itertools import product
from Crypto.Hash import keccak

alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()+'

similar_hash = 'c8e91ccb01ea55ea98b132bb9cdd62c87a2a19983a4038b505eb5a1b'
salt = '0a831d4e-3c01-4783-a54a-302a405b2ae6'

found = False
pass_length = 0

while not found:
    pass_length += 1

    combinations = product(alphabet, repeat=pass_length)

    for combination in combinations:
        password = ''.join(combination)

        my_hash = keccak.new(data=(password+salt).encode('utf-8'), digest_bits=224).hexdigest()

        if my_hash == similar_hash:
            print(f'password = {password}, hash = {my_hash}')
            found = True
            break

# password = UspX, hash = c8e91ccb01ea55ea98b132bb9cdd62c87a2a19983a4038b505eb5a1b