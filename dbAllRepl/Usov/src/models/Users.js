const dBConnectionPG = require('../utils/DBConnectionsPG');
const rdbConnectionPG = require('../utils/ReplicationDBConnectionsPG');
const dBConnectionRD = require('../utils/DBConnectionsRedis');

class Users {
	async getAllUsers() {
		try {
			const result = await dBConnectionPG
				.getConnections()
				.query('SELECT * FROM users');
			return result.rows;
		} catch (error) {
			console.log(error);
		}
	}

	async getAllReplicationUsers() {
		try {
			const result = await rdbConnectionPG
				.getConnections()
				.query('SELECT * FROM users');
			return result.rows;
		} catch (error) {
			console.log(error);
		}
	}

	async getUserById(id) {
		try {
			const redis_key = 'user' + id;
			const redis_value = await dBConnectionRD.getConnections().get(redis_key);
			if (redis_value === null) {
				const result = await dBConnectionPG
					.getConnections()
					.query(`SELECT * FROM users WHERE users.id = $1`, [id]);
				await dBConnectionRD
					.getConnections()
					.set('user' + id, JSON.stringify(result.rows[0]));
				return result.rows[0];
			} else {
				return JSON.parse(redis_value);
			}
		} catch (error) {
			console.log(error);
		}
	}

	async creatUser(name, age, phone) {
		try {
			const result = await dBConnectionPG
				.getConnections()
				.query(
					`INSERT INTO users(name, age, phone) values($1, $2, $3) returning *;`,
					[name, age, phone]
				);
			return result.rows[0];
		} catch (error) {
			console.log(error);
		}
	}

	async updateUser(id, name, age, phone) {
		try {
			const result = await dBConnectionPG
				.getConnections()
				.query(
					`UPDATE users SET name = $1, age = $2, phone = $3 WHERE id = $4 returning *;`,
					[name, age, phone, id]
				);
			return result.rows[0];
		} catch (error) {
			console.log(error);
		}
	}

	async deleteUserById(id) {
		try {
			const result = await dBConnectionPG
				.getConnections()
				.query(`DELETE FROM users WHERE users.id = $1 returning *`, [id]);
			return result.rows;
		} catch (error) {
			console.log(error);
		}
	}
}

module.exports = new Users();
