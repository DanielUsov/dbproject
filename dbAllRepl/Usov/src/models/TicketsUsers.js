const dBConnectionPG = require('../utils/DBConnectionsPG');

class TicketsUsers {
	async getAll() {
		try {
			const result = await dBConnectionPG
				.getConnections()
				.query('SELECT * FROM tickets_users');
			return result.rows;
		} catch (error) {
			console.log(error);
		}
	}

	async create(ticketsId, usersId) {
		try {
			const result = await dBConnectionPG
				.getConnections()
				.query(
					'INSERT INTO tickets_users(tickets_id, users_id) values($1, $2) returning *;',
					[ticketsId, usersId]
				);
			return result.rows;
		} catch (error) {
			console.log(error);
		}
	}
}

module.exports = new TicketsUsers();
