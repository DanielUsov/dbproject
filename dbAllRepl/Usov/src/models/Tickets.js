const dBConnectionPG = require('../utils/DBConnectionsPG');
const dBConnectionRD = require('../utils/DBConnectionsRedis');

class Tickets {
	async getAllTickets() {
		try {
			const result = await dBConnectionPG
				.getConnections()
				.query('SELECT * FROM tickets');
			return result.rows;
		} catch (error) {
			console.log(error);
		}
	}

	async getTicketById(id) {
		try {
			const redis_key = 'ticket' + id;
			const redis_value = await dBConnectionRD.getConnections().get(redis_key);
			if (redis_value === null) {
				const result = await dBConnectionPG
					.getConnections()
					.query(`SELECT * FROM tickets WHERE tickets.id = $1`, [id]);
				await dBConnectionRD
					.getConnections()
					.set('ticket' + id, JSON.stringify(result.rows[0]));
				return result.rows[0];
			} else {
				return JSON.parse(redis_value);
			}
		} catch (error) {
			console.log(error);
		}
	}

	async createTicket(filmTitle, filmYear, places, price) {
		try {
			const result = await dBConnectionPG
				.getConnections()
				.query(
					`INSERT INTO tickets (film_id, places, price) values ((SELECT films.id FROM films WHERE films.title = $1 AND films.year = $2), $3, $4) returning *;`,
					[filmTitle, filmYear, places, price]
				);
			return result.rows[0];
		} catch (error) {
			console.log(error);
		}
	}

	async updateTicket(id, filmTitle, filmYear, places, price) {
		try {
			const result = await dBConnectionPG
				.getConnections()
				.query(
					`UPDATE tickets SET film_id = (SELECT films.id FROM films WHERE films.title = $1 AND films.year = $2), places = $3, price = $4 WHERE tickets.id = $5 returning *;`,
					[filmTitle, filmYear, places, price, id]
				);
			return result.rows[0];
		} catch (error) {
			console.log(error);
		}
	}

	async deleteTicket(id) {
		try {
			const result = await dBConnectionPG
				.getConnections()
				.query(`DELETE FROM tickets WHERE id = $1 returning *;`, [id]);
			return result.rows[0];
		} catch (error) {
			console.log(error);
		}
	}
}

module.exports = new Tickets();
