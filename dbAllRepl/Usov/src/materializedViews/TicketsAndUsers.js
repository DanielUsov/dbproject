const dBConnectionPG = require('../utils/DBConnectionsPG');

class TicketsAndUsers {
	async getTicketsAndUsers() {
		try {
			const result = await dBConnectionPG
				.getConnections()
				.query('SELECT * FROM tickets_and_users');
			return result.rows;
		} catch (error) {
			console.log(error);
		}
	}

	async refresh() {
		try {
			await dBConnectionPG
				.getConnections()
				.query('refresh materialized view tickets_and_users;');
			return 'refresh materialized view successful';
		} catch (error) {
			console.log(error);
		}
	}

	async getMateriz(name, places) {
		try {
			const result = await dBConnectionPG
				.getConnections()
				.query(
					`SELECT * FROM tickets_and_users WHERE tickets_and_users.name = $1 AND tickets_and_users.places = $2`,
					[name, places]
				);
			return result.rows;
		} catch (error) {
			console.log(error);
		}
	}

	async getUsersByIndexs(name, age, phone) {
		try {
			const result = await dBConnectionPG
				.getConnections()
				.query(
					`SELECT * FROM users WHERE users.name = $1 AND users.age = $2 AND users.phone = $2`,
					[name, age, phone]
				);
			return result.rows;
		} catch (error) {
			console.log(error);
		}
	}
}

module.exports = new TicketsAndUsers();
