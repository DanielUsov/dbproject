require('dotenv').config();

class ELConnections {
	getConnections() {
		try {
			const { Client } = require('@elastic/elasticsearch');
			const client = new Client({
				node: `http://elasticsearch:9200`,
			});
			return client;
		} catch (error) {
			console.log(error);
		}
	}
}

module.exports = new ELConnections();
