require('dotenv').config();

class DBConnectionsRedis {
	getConnections() {
		try {
			const { createClient } = require('redis');
			const redisClient = new createClient({
				url: `redis://@${process.env.REDIS_HOST}:${process.env.REDIS_PORT}`,
			});
			redisClient.on('error', (err) => {
				console.log(err);
			});
			redisClient.connect();
			return redisClient;
		} catch (error) {
			console.log(error);
		}
	}
}

module.exports = new DBConnectionsRedis();
