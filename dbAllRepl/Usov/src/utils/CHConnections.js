require('dotenv').config();

class CHConnections {
	getConnections() {
		try {
			const { ClickHouse } = require('clickhouse');
			const clickhouse = new ClickHouse({
				user: process.env.CLICKHOUSE_USER,
				host: process.env.CLICKHOUSE_HOST || 'localhost',
				password: process.env.CLICKHOUSE_PASSWORD,
				database: 'postgres_repl',
				port: process.env.CLICKHOUSE_PORT,
			});
			return clickhouse;
		} catch (error) {
			console.log(error);
		}
	}
}

module.exports = new CHConnections();
