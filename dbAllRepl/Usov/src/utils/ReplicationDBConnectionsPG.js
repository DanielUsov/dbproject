require('dotenv').config();

class ReplicationDBConnectionsPG {
	getConnections() {
		const { Pool } = require('pg');
		const pool = new Pool({
			user: process.env.POSTGRES_USER,
			password: process.env.POSTGRES_PASSWORD,
			host: process.env.SLAVE_HOST,
			port: process.env.SLAVE_PORT,
			database: process.env.POSTGRES_DB,
		});
		return pool;
	}
}

module.exports = new ReplicationDBConnectionsPG();
