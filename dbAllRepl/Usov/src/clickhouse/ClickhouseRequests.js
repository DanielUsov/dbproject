const chConnections = require('../utils/CHConnections');

class ClickhouseRequests {
	async clickhouse1() {
		try {
			const result = await chConnections
				.getConnections()
				.query(
					`select min(runtime) as min_runtime, max(runtime) as max_runtime, avg(runtime) as avg_runtime, metacritic from films group by metacritic`
				)
				.toPromise();
			console.log(result);
			return result;
		} catch (error) {
			console.log(error);
		}
	}

	async clickhouse2() {
		try {
			const result = await chConnections
				.getConnections()
				.query(
					`SELECT floor((price) / 100) * 100 AS min_price, (floor((price) / 100) + 1) * 100 AS max_price FROM tickets GROUP BY floor((price) / 100) ORDER BY floor((price) / 100);`
				)
				.toPromise();
			console.log(result);
			return result;
		} catch (error) {
			console.log(error);
		}
	}
}

module.exports = new ClickhouseRequests();
