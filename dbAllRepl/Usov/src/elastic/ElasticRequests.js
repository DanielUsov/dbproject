const elConnection = require('../utils/ELConnections');

class ElasticRequests {
	async getAllFilmsFromElastic() {
		try {
			const result = await elConnection.getConnections().search({
				index: 'films',
				query: {
					query_string: {
						query: '*',
					},
				},
			});
			return result.hits.hits;
		} catch (error) {
			console.log(error);
		}
	}

	async getFilmsFromElasticByRated() {
		try {
			const result = await elConnection.getConnections().search({
				index: 'films',
				query: {
					term: {
						rated: 'G',
					},
				},
			});
			return result.hits.hits;
		} catch (error) {
			console.log(error);
		}
	}

	async getFilmsFromElastic() {
		try {
			const result = await elConnection.getConnections().search({
				index: 'films',
				body: {
					aggs: {
						by_metacritic: {
							histogram: {
								field: 'metacritic',
								interval: 1000,
							},
							aggs: {
								total_metacritic: {
									sum: {
										script:
											"return doc['metacritic'].value * doc['lobby'].value",
									},
								},
							},
						},
					},
					size: 0,
				},
			});
			return result.aggregations;
		} catch (error) {
			console.log(error);
		}
	}

	async autocomplete(word) {
		try {
			const result = await elConnection.getConnections().search({
				index: 'films',
				query: {
					match: {
						title: {
							query: word,
							analyzer: 'my_ngram_analyzer',
						},
					},
				},
			});
			return result.hits.hits;
		} catch (error) {
			console.log(error);
		}
	}
}

module.exports = new ElasticRequests();
