CREATE TABLE tickets_users (
    tickets_id BIGINT REFERENCES tickets,
    users_id BIGINT REFERENCES users,
    PRIMARY KEY (tickets_id, users_id)
);
