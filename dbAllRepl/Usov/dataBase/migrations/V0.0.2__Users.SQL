CREATE TABLE users (
    id BIGINT generated always as identity primary key,
    name text not null,
    age int not null,
    phone text not null
);

