INSERT INTO tickets_users(tickets_id, users_id)
values (
        (
            select tickets.id
            from tickets
            where tickets.film_id = (
                    select films.id
                    from films
                    where films.title = 'The Martian'
                )
        ),
        (
            select users.id
            from users
            where users.name = 'Данилов Г.К'
        )
    ),
    (
        (
            select tickets.id
            from tickets
            where tickets.film_id = (
                    select films.id
                    from films
                    where films.title = 'Toy Story 3'
                )
        ),
        (
            select users.id
            from users
            where users.name = 'Рындин С.И'
        )
    ),
    (
        (
            select tickets.id
            from tickets
            where tickets.film_id = (
                    select films.id
                    from films
                    where films.title = 'Hessians MC'
                )
        ),
        (
            select users.id
            from users
            where users.name = 'Усов Д.А'
        )
    );