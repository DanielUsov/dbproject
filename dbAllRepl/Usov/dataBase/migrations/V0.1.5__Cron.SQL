CREATE extension pg_cron;

SELECT
    cron.schedule(
        'tickets_and_users',
        '* * * * *',
        $$ 
            refresh materialized VIEW concurrently tickets_and_users 
        $$
    );
