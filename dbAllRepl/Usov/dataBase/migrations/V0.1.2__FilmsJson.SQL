ALTER TABLE
    films
ADD
    jsonFormat jsonb;

INSERT INTO
    films(jsonFormat)
VALUES
    (
        '[
				{
					"title": "Shu jian en chou lu",
                    "year": "1987",
					"rated": "PG-13",
					"runtime": "120",
                    "plot": "The story is based on the popular novel developed from folk legend. It goes that the Manchurian emperor Qianlong of China (circa 18th Century) was actually the son of a Han Chinese, the ...",
                    "metacritic": "62"
				}
			]'
    );

CREATE INDEX films_jsonFormat ON films USING gin (jsonFormat);
