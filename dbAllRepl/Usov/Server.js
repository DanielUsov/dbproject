'use strict';
require('dotenv').config();

const user = require('./src/models/Users');
const film = require('./src/models/Films');
const ticket = require('./src/models/Tickets');
const ticketsAndUsers = require('./src/materializedViews/TicketsAndUsers');
const ticketsUsers = require('./src/models/TicketsUsers');
const elastic = require('./src/elastic/ElasticRequests');
const clickhouse = require('./src/clickhouse/ClickhouseRequests');

const PORT = process.env.APP_PORT || 3000;

const express = require('express');
const morgan = require('morgan');
const path = require('path');
const app = express();

app.use(morgan('dev'));
app.use(express.json());

// ---------------------------------------------------------------------

app.get('/Users', async (req, res) => {
	const result = await user.getAllUsers();
	res.json(result);
});

app.get('/ReplicationUsers', async (req, res) => {
	const result = await user.getAllReplicationUsers();
	res.json(result);
});

app.get('/User/:id', async (req, res) => {
	const { id } = req.params;
	const result = await user.getUserById(Number(id));
	res.json(result);
});

app.post('/User', async (req, res) => {
	const { name, age, phone } = req.body;
	const result = await user.creatUser(String(name), Number(age), String(phone));
	res.json(result);
});

app.put('/User/:id', async (req, res) => {
	const { id } = req.params;
	const { name, age, phone } = req.body;
	const result = await user.updateUser(
		Number(id),
		String(name),
		Number(age),
		String(phone)
	);
	res.json(result);
});

app.delete('/User/:id', async (req, res) => {
	const { id } = req.params;
	const result = await user.deleteUserById(Number(id));
	res.json(result);
});

// ---------------------------------------------------------------------

app.get('/Films', async (req, res) => {
	const result = await film.getAllFilms();
	res.json(result);
});

app.get('/Film/:id', async (req, res) => {
	const { id } = req.params;
	const result = await film.getFilmById(Number(id));
	res.json(result);
});

app.get('/json', async (req, res) => {
	const { year } = req.body;
	const result = await film.getFilmsJsonb(year);
	res.json(result);
});

app.get('/array', async (req, res) => {
	const { review } = req.body;
	const result = await film.getFilmsArray(review);
	res.json(result);
});

app.post('/Film', async (req, res) => {
	const { title, year, rated, runtime, plot, metacritic } = req.body;
	const result = await film.creatFilm(
		String(title),
		Number(year),
		String(rated),
		Number(runtime),
		String(plot),
		Number(metacritic)
	);
	res.json(result);
});

app.put('/Film/:id', async (req, res) => {
	const { id } = req.params;
	const { title, year, rated, runtime, plot, metacritic } = req.body;
	const result = await film.updateFilm(
		Number(id),
		String(title),
		Number(year),
		String(rated),
		Number(runtime),
		String(plot),
		Number(metacritic)
	);
	res.json(result);
});

app.delete('/Film/:id', async (req, res) => {
	const { id } = req.params;
	const result = await film.deleteFilm(Number(id));
	res.json(result);
});

// ---------------------------------------------------------------------

app.get('/Tickets', async (req, res) => {
	const result = await ticket.getAllTickets();
	res.json(result);
});

app.get('/Ticket/:id', async (req, res) => {
	const { id } = req.params;
	const result = await ticket.getTicketById(Number(id));
	res.json(result);
});

app.post('/Ticket', async (req, res) => {
	const { filmTitle, filmYear, places, price } = req.body;
	const result = await ticket.createTicket(
		Number(filmTitle),
		Number(filmYear),
		places,
		Number(price)
	);
	res.json(result);
});

app.put('/Ticket', async (req, res) => {
	const { id, filmTitle, filmYear, places, price } = req.body;
	const result = await ticket.updateTicket(
		Number(id),
		Number(filmTitle),
		Number(filmYear),
		places,
		Number(price)
	);
	res.json(result);
});

app.delete('/Ticket/:id', async (req, res) => {
	const { id } = req.params;
	const result = await ticket.deleteTicket(Number(id));
	res.json(result);
});

// ---------------------------------------------------------------------

app.get('/getTicketsAndUsers', async (req, res) => {
	const result = await ticketsAndUsers.getTicketsAndUsers();
	res.json(result);
});

app.get('/refresh', async (req, res) => {
	const result = await ticketsAndUsers.refresh();
	res.json(result);
});

app.get('/MaterizWithIndexs', async (req, res) => {
	const { name, places } = req.body;
	const result = await ticketsAndUsers.getMateriz(name, places);
	res.json(result);
});

app.get('/UsersByIndexs', async (req, res) => {
	const { name, age, phone } = req.body;
	const result = await ticketsAndUsers.getUsersByIndexs(name, age, phone);
	res.json(result);
});

// ---------------------------------------------------------------------

app.get('/TicketsUsers', async (req, res) => {
	const result = await ticketsUsers.getAll();
	res.json(result);
});

app.post('/TicketUser', async (req, res) => {
	const { tickets_id, users_id } = req.body;
	const result = await ticketsUsers.create(tickets_id, users_id);
	res.json(result);
});

// ---------------------------------------------------------------------

app.get('/FilmsFromElastic', async (req, res) => {
	const result = await elastic.getAllFilmsFromElastic();
	res.json(result);
});

app.get('/FilmsFromElasticByRated', async (req, res) => {
	const result = await elastic.getFilmsFromElasticByRated();
	res.json(result);
});

app.get('/FilmsFromElasticByMetacriticAndLobby', async (req, res) => {
	const result = await elastic.getFilmsFromElastic();
	res.json(result);
});

app.get('/autocomplete', async (req, res) => {
	const result = await elastic.autocomplete(req.query.word);
	res.json(result.map((obj) => obj['_source']['title']));
});

app.get('/main', async (req, res) => {
	res.sendFile(path.join(__dirname, '/static/autocomplete.html'));
});

// ---------------------------------------------------------------------

app.get('/clickhouse1', async (req, res) => {
	const result = await clickhouse.clickhouse1();
	res.json(result);
});

app.get('/clickhouse2', async (req, res) => {
	const result = await clickhouse.clickhouse2();
	res.json(result);
});

// ---------------------------------------------------------------------

app.listen(PORT, function () {
	console.log(`Server listening port ${PORT}`);
});
