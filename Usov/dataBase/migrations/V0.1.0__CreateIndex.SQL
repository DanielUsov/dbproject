CREATE unique index tickets_users_ids on tickets_users using btree (tickets_id, users_id);

CREATE unique index film_and_users_ids on tickets_and_users using btree (users_id, tickets_id);

CREATE index tickets_and_users_by_name_and_places on tickets_and_users using btree (name, places);

CREATE index user_by_name_and_age_and_phone on users using btree (name, age, phone);