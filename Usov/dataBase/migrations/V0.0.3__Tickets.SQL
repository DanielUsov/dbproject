CREATE TABLE tickets (
    id BIGINT generated always as identity primary key,
    film_id int references films,
    places int not null,
    price money not null
);
