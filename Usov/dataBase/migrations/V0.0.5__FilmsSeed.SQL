INSERT into films (title, year, rated, runtime, plot, metacritic)
values (
        'The Martian',
        2015,
        'PG-13',
        144,
        'During a manned mission to Mars',
        92
    ),
    (
        'Toy Story 3',
        2010,
        'G',
        103,
        'The toys are mistakenly delivered to a day-care center instead of the attic right before Andy leaves for college, and its up to Woody to convince the other toys that they werent abandoned and to return home.',
        92
    ),
    (
        'Hessians MC',
        2005,
        'PG-13',
        75,
        'The violent legacy of this hard-riding, hard-partying and hard-fighting outlaw biker club has endured nearly four decades. The Hessians share their personal stories, anecdotes and the ...',
        89
    ),
    (
        'Once Upon a Time in the West',
        1968,
        'PG-13',
        175,
        'Epic story of a mysterious stranger with a harmonica who joins forces with a notorious desperado to protect a beautiful widow from a ruthless assassin working for the railroad.',
        80
    ),
    (
        'Wild Wild West',
        1999,
        'PG-13',
        106,
        'The two best hired guns in the West must save President Grant from the clutches of a nineteenth-century inventor-villain.',
        38
    );