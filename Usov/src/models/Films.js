const dBConnectionPG = require('../utils/DBConnectionsPG');
const dBConnectionRD = require('../utils/DBConnectionsRedis');

class Films {
	async getAllFilms() {
		try {
			const result = await dBConnectionPG
				.getConnections()
				.query('SELECT * FROM films');
			return result.rows;
		} catch (error) {
			console.log(error);
		}
	}

	async getFilmById(id) {
		try {
			const redis_key = 'film' + id;
			const redis_value = await dBConnectionRD.getConnections().get(redis_key);
			if (redis_value === null) {
				const result = await dBConnectionPG
					.getConnections()
					.query('SELECT * FROM films WHERE id = $1', [id]);
				await dBConnectionRD
					.getConnections()
					.set('film' + id, JSON.stringify(result.rows[0]));
				return result.rows[0];
			} else {
				return JSON.parse(redis_value);
			}
		} catch (error) {
			console.log(error);
		}
	}

	async getFilmByTitleAndYear(title, year) {
		try {
			const result = await dBConnectionPG
				.getConnections()
				.query(`SELECT * FROM films WHERE title = $1 AND year = $2`, [
					title,
					year,
				]);
			return result.rows[0];
		} catch (error) {
			console.log(error);
		}
	}

	async creatFilm(title, year, rated, runtime, plot, metacritic) {
		try {
			const result = await dBConnectionPG
				.getConnections()
				.query(
					`INSERT into films (title, year, rated, runtime, plot, metacritic) values ( $1, $2, $3, $4, $5, $6) returning *;`,
					[
						String(title),
						Number(year),
						String(rated),
						Number(runtime),
						String(plot),
						Number(metacritic),
					]
				);
			return result.rows[0];
		} catch (error) {
			console.log(error);
		}
	}

	async updateFilm(id, title, year, rated, runtime, plot, metacritic) {
		try {
			const result = await dBConnectionPG
				.getConnections()
				.query(
					`UPDATE films SET title = $1, year = $2, rated = $3, runtime = $4, plot = $5, metacritic = $6 WHERE id = $7 returning *;`,
					[
						String(title),
						Number(year),
						String(rated),
						Number(runtime),
						String(plot),
						Number(metacritic),
						Number(id),
					]
				);
			return result.rows[0];
		} catch (error) {
			console.log(error);
		}
	}

	async deleteFilm(id) {
		try {
			const result = await dBConnectionPG
				.getConnections()
				.query('DELETE FROM films WHERE id = $1 returning *;', [id]);
			return result.rows;
		} catch (error) {
			console.log(error);
		}
	}
}

module.exports = new Films();
